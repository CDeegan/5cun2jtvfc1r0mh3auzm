import type { Config } from 'jest';

const config: Config = {
  moduleNameMapper: { '@code/(.*)': '<rootDir>/src/$1' },
  preset: 'ts-jest'
};

export default config;
