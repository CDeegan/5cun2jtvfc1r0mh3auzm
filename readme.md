# Weather Metrics API
This is a REST API that receives weather data from sensors and allows that data to be queried with specificity.

[API Documentation](https://documenter.getpostman.com/view/446215/2sA2r8145G)

## Specification
See [spec](spec.md).

## Dependencies
- Node.js v20
- Timescale Database v16

OR
- Docker v21+

## Environment
To run this project, certain environment variables are required. These are outlined in a .env.example file. For running via Docker Compose, the commented lines should be uncommented.

## Running the project
By default the server will run at `http://localhost:3000.`

### Docker Compose (recommended)
1. Clone the project
2. Open the project directory in your terminal
3. Create a `.env` file with the relevant data
4. Run `docker compose up`

### Via Node
1. Clone the project
2. Start your target [Timescale instance](https://docs.timescale.com/self-hosted/latest/install/)
3. Populate your Timescale instance with data from `scripts/init.sql`
4. Open the project directory in your terminal
5. Create a `.env` file with the relevant data
6. Run `npm install`
7. Run `npm run build`
8. Run `npm run start`

## Running Tests
To run tests, run `npm test`.

## Notes about the challenge
See [notes](challenge.md).
