Imagine that we are building a service that receives weather data from various sensors that
report metrics such as temperature, humidity, wind speed, etc. Your task is building an application that solves part of this problem:
- The application can receive new metric values as the weather changes around the
sensor via an API call.
- The application must allow querying sensor data. A query should define:
  - One or more (or all sensors) to include in results.
  - The metrics (e.g. temperature and humidity); the application should return
the average value for these metrics.
  - The statistic for the metric: min, max, sum or average.
  - A date range (between one day and a month, if not specified, the latest data
should be queried).
  - Example query: Give me the average temperature and humidity for sensor 1
in the last week.