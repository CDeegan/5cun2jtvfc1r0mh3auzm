## Library Choices
- Zod is used to validate incoming requests and ensure they conform to a specific structure. It enforces strong type checking, making it ideal for any TypeScript project. In the past I have used AJV or JOI libraries to achieve the same thing.
- Kysely is a query builder used to interface with the database. I prefer this over ORMs due to the flexibility and a preference for not abstracting SQL too heavily. In other projects I have used the Knex query builder library, and Kysely is essentially a TypeScript-first variant of Knex.

## Design Choices
- The "sensorId" property of the WeatherSensorData object would refer to a WeatherSensor object with the corresponding "id". This WeatherSensor object would have its own table/api, but for the purpose of this exercise it has not been included
  - This "sensorId" is currently a UUID string since using integer IDs does not scale well in distributed systems
  - Ideally the UUID could be stored as binary (unwrapped with BIN_TO_UUID()) as it has better query performance than string (especially when UUIDs are reversed).
- Calculation of statistics is done in the DB layer. The idea behind this is to offload computation to Timescale, which is more performant at this kind of task than Node.js. Downsides include:
  - application can scale horizontally, parallel Node clusters/processes/instances could handle this computation and prevent any blocking behaviour
  - mixing business logic into database layer could be considered less convenient
- TimescaleDB was used as a database considering the nature of the data (time-series) being stored and the way it gets queried (by time range).
  - allows for querying by "time buckets", so you could get statistics over a certain period of time, e.g. get the daily average temperature recorded by sensor X in the past 14 days
  - allows for more complex data analysis via "hyperfunctions", e.g. percentile approximation, time-weighted averages, statistical aggregation
  - "hypertables" that automatically partition data by time and chunks data into child tables

## Scaling Considerations
- Post server logs to external resource
- Data could be ingested via Kafka instead of direct to API
- Horizontal scaling: many instances of the same API behind a load balancer
- Reduce data transfer by paginating results
- Caching
  - cache-aside pattern
    - check the cache for a matching query
    - if none exists
      - query the database
      - store query + result in cache
    - if exists (same query was made recently)
      - read direct from cache
  - redis has support for time-series data, a cache of historical data is built up as users query for data
  - timescale also supports continuous aggregates, i.e. automatically updating views

## Security Considerations
- Rate limiting/throttling to mitigate abuse
- CORS
- Authentication (e.g. via JWT, API Key)
- TLS (allow some gateway/load balancer to handle SSL termination)