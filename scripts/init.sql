CREATE DATABASE weather_sensor;

\c weather_sensor;

CREATE TABLE IF NOT EXISTS log (
  "time" timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
  "sensorId" UUID NOT NULL,
  "metric" TEXT NOT NULL,
  "value" REAL NOT NULL
);

SELECT create_hypertable('log', by_range('time'));

INSERT INTO log VALUES ('2024-02-01 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'temperature', 21.1);
INSERT INTO log VALUES ('2024-02-02 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'temperature', 21.6);
INSERT INTO log VALUES ('2024-02-03 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'temperature', 22.4);
INSERT INTO log VALUES ('2024-02-04 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'temperature', 19.8);
INSERT INTO log VALUES ('2024-02-05 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'temperature', 20.2);
INSERT INTO log VALUES ('2024-02-06 00:02:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'temperature', 20.5);
INSERT INTO log VALUES ('2024-02-07 00:02:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'temperature', 21.1);
INSERT INTO log VALUES ('2024-02-01 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'humidity', 4.4382);
INSERT INTO log VALUES ('2024-02-02 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'humidity', 4.2098);
INSERT INTO log VALUES ('2024-02-03 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'humidity', 4.7571);
INSERT INTO log VALUES ('2024-02-04 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'humidity', 4.191);
INSERT INTO log VALUES ('2024-02-05 00:00:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'humidity', 4.1963);
INSERT INTO log VALUES ('2024-02-06 00:02:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'humidity', 3.4912);
INSERT INTO log VALUES ('2024-02-07 00:02:00.000000 +00:00', '2ed8dbf0-e5bb-478c-a432-311044dbd16c', 'humidity', 4.3585);

INSERT INTO log VALUES ('2024-02-01 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'radiation', 0.06);
INSERT INTO log VALUES ('2024-02-02 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'radiation', 0.05);
INSERT INTO log VALUES ('2024-02-03 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'radiation', 0.06);
INSERT INTO log VALUES ('2024-02-04 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'radiation', 0.05);
INSERT INTO log VALUES ('2024-02-05 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'radiation', 0.05);
INSERT INTO log VALUES ('2024-02-06 00:02:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'radiation', 0.07);
INSERT INTO log VALUES ('2024-02-07 00:02:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'radiation', 0.05);

INSERT INTO log VALUES ('2024-02-01 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'humidity', 0.06);
INSERT INTO log VALUES ('2024-02-02 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'humidity', 0.05);
INSERT INTO log VALUES ('2024-02-03 00:00:00.000000 +00:00', '5e89edfb-d6d3-4e2d-aa88-7c76ae3a7e6c', 'humidity', 0.06);

INSERT INTO log VALUES ('2024-02-01 01:00:00.000000 +00:00', '4538ef51-e4f1-5fd9-86d4-ebe65cbd700d', 'temperature', 13.634);
INSERT INTO log VALUES ('2024-02-02 02:00:00.000000 +00:00', '827ecf2e-6e87-56b0-948f-9df656bdc0fd', 'temperature', 13.2263);
INSERT INTO log VALUES ('2024-02-03 03:00:00.000000 +00:00', 'a430ce8e-5bab-5c08-844b-6ae97c1b9db8', 'temperature', 13.2066);
