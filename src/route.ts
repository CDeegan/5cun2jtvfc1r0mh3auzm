import { Application, Router } from 'express';
import sensorMetric from '@code/components/sensor-metric/sensor-metric.route';

export default ({ app }: { app: Application }) => {
  const apiRouter = Router();
  sensorMetric(apiRouter);

  app.use('/api', apiRouter);

  return app;
};
