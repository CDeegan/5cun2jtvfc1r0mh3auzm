import { LogTable } from '@code/components/sensor-metric/sensor-metric.db';
import { Kysely, PostgresDialect, sql } from 'kysely';
import { Pool } from 'pg';

export interface Database {
  log: LogTable;
}

const dialect = new PostgresDialect({
  pool: new Pool({
    database: process.env.DB_TARGET,
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    port: parseInt(process.env.DB_PASSWORD as string, 10),
    max: 10
  })
});

export const db = new Kysely<Database>({
  dialect
});

(async () => {
  const result = (await sql`SELECT VERSION()`.execute(db)) as { rows?: { version: string }[] };
  if (result.rows && result.rows.length) {
    console.log('Database ready', result.rows[0].version);
  }
})();
