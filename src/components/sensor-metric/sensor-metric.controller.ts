import { SensorDataSchema, SensorQuerySchema } from '@code/components/sensor-metric/sensor-metric.model';
import { NextFunction, Request, Response } from 'express';
import { createMetricLog, querySensorMetric } from './sensor-metric.db';
import { badImplementation } from '@hapi/boom';

export const getSensorMetric = async (req: Request, res: Response, next: NextFunction) => {
  const sensorMetricQuery = SensorQuerySchema.parse(req.query);
  try {
    const result = await querySensorMetric(sensorMetricQuery);
    return res.json(result);
  } catch (e) {
    return next(badImplementation(e as Error));
  }
};

export const createSensorMetric = async (req: Request, res: Response, next: NextFunction) => {
  const newSensorMetric = SensorDataSchema.parse(req.body);
  try {
    const result = await createMetricLog(newSensorMetric);
    return res.json(result);
  } catch (e) {
    return next(badImplementation(e as Error));
  }
};
