import { validateRequest } from '@code/middleware/validator';
import { Router } from 'express';
import { SensorMetricCreateRequestSchema, SensorMetricQueryRequestSchema } from '@code/components/sensor-metric/sensor-metric.model';
import { createSensorMetric, getSensorMetric } from '@code/components/sensor-metric/sensor-metric.controller';

const route = Router();
export default (app: Router) => {
  route.get('/', validateRequest(SensorMetricQueryRequestSchema), getSensorMetric);
  route.post('/', validateRequest(SensorMetricCreateRequestSchema), createSensorMetric);
  app.use('/sensor/metric', route);
};
