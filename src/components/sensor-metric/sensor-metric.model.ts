import { z } from 'zod';

const SENSOR_METRIC_VALUES = ['temperature', 'humidity', 'wind', 'radiation'] as const;
const SensorMetricEnum = z.enum(SENSOR_METRIC_VALUES);

const SENSOR_DEFAULT_AGGREGATE_VALUE = 'average';
const SENSOR_AGGREGATE_VALUES = ['average', 'min', 'max', 'sum'] as const;
const SensorAggregateEnum = z.enum(SENSOR_AGGREGATE_VALUES);

export const SensorDataSchema = z.object({
  sensorId: z.string().uuid(),
  metric: SensorMetricEnum,
  value: z.number()
});

export const SensorQuerySchema = z.object({
  metric: z.union([SensorMetricEnum, z.array(SensorMetricEnum)]),
  sensorId: z.union([z.string().uuid(), z.array(z.string().uuid())]).optional(),
  aggregate: SensorAggregateEnum.default(SENSOR_DEFAULT_AGGREGATE_VALUE),
  since: z.coerce.number().min(1).max(31).default(1)
});

const SensorQueryResultSchema = z.object({
  sensorId: z.string().uuid(),
  startDate: z.string().datetime(),
  endDate: z.string().datetime(),
  aggregate: SensorAggregateEnum,
  data: z.array(
    z.object({
      metric: SensorMetricEnum,
      value: z.number()
    })
  )
});

export const SensorMetricCreateRequestSchema = z.object({
  body: SensorDataSchema
});

export const SensorMetricQueryRequestSchema = z.object({
  query: SensorQuerySchema
});

export type SensorMetricEnumType = z.infer<typeof SensorMetricEnum>;
export type SensorDataType = z.infer<typeof SensorDataSchema>;
export type SensorQueryType = z.infer<typeof SensorQuerySchema>;
export type SensorQueryResultType = z.infer<typeof SensorQueryResultSchema>;
