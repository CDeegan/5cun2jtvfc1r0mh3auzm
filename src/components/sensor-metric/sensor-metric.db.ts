import { FunctionModule, Generated, Insertable, Selectable, sql } from 'kysely';
import { Database, db } from '@code/database';
import { SensorMetricEnumType, SensorQueryType } from './sensor-metric.model';

export interface LogTable {
  time: Generated<Date>;
  sensorId: string;
  metric: SensorMetricEnumType;
  value: number;
}

type Log = Selectable<LogTable>;
type NewLog = Insertable<LogTable>;

function getAggregateFunction(fn: FunctionModule<Database, 'log'>, queryAggregate: string) {
  switch (queryAggregate) {
    case 'sum':
      return fn.sum('value').as(queryAggregate);
    case 'max':
      return fn.max('value').as(queryAggregate);
    case 'min':
      return fn.min('value').as(queryAggregate);
    default:
      return fn.avg('value').as(queryAggregate);
  }
}

export async function createMetricLog(log: NewLog): Promise<Log> {
  return await db.insertInto('log').values(log).returningAll().executeTakeFirstOrThrow();
}

export async function querySensorMetric(query: SensorQueryType) {
  const metricList = Array.isArray(query.metric) ? query.metric : [query.metric];
  let dbQuery = db
    .selectFrom('log')
    .select(({ fn }) => [
      'sensorId',
      sql<Date>`now() - (${query.since} || 'days')::interval`.as('startDate'),
      sql<Date>`now()`.as('endDate'),
      'metric',
      getAggregateFunction(fn, query.aggregate)
    ])
    .where('time', '>', sql<Date>`now() - (${query.since} || 'days')::interval`)
    .where('metric', 'in', metricList)
    .groupBy(['sensorId', 'metric']);

  if (query.sensorId) {
    const sensorIdList = Array.isArray(query.sensorId) ? query.sensorId : [query.sensorId];
    dbQuery = dbQuery.where('sensorId', 'in', sensorIdList);
  }

  return await dbQuery.execute();
}
