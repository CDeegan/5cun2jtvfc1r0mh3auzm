import { ZodError } from 'zod';
import { SensorMetricCreateRequestSchema, SensorMetricQueryRequestSchema } from '../sensor-metric.model';

describe('weather-sensor-metric model validations', () => {
  test('should reject invalid POST request body (incorrect id form)', () => {
    const req = {
      body: {
        sensorId: 1,
        metric: 'humidity',
        value: 100
      }
    };

    expect(() => {
      SensorMetricCreateRequestSchema.parse(req);
    }).toThrow(ZodError);
  });

  test('should reject invalid POST request body (unknown metric)', () => {
    const req = {
      body: {
        sensorId: '4e2a6260-1add-4855-8737-89845ec1a49e',
        metric: 'some-invalid-metric',
        value: 100
      }
    };

    expect(() => {
      SensorMetricCreateRequestSchema.parse(req);
    }).toThrow(ZodError);
  });

  test('should accept valid POST request body', () => {
    const req = {
      body: {
        sensorId: '4e2a6260-1add-4855-8737-89845ec1a49e',
        metric: 'temperature',
        value: 100
      }
    };

    expect(() => {
      SensorMetricCreateRequestSchema.parse(req);
    }).not.toThrow();
  });

  test('should reject invalid GET query (invalid metric)', () => {
    const req = {
      query: {
        sensorId: '4e2a6260-1add-4855-8737-89845ec1a49e',
        metric: ['radiation', 'some-invalid-metric'],
        aggregate: 'max',
        since: 2
      }
    };

    expect(() => {
      SensorMetricQueryRequestSchema.parse(req);
    }).toThrow(ZodError);
  });

  test('should reject invalid GET query (invalid aggregate)', () => {
    const req = {
      query: {
        sensorId: '4e2a6260-1add-4855-8737-89845ec1a49e',
        metric: ['radiation', 'temperature'],
        aggregate: 'some-invalid-aggregate',
        since: 2
      }
    };

    expect(() => {
      SensorMetricQueryRequestSchema.parse(req);
    }).toThrow(ZodError);
  });

  test('should default GET query "since" value to 1', () => {
    const req = {
      query: {
        sensorId: ['4e2a6260-1add-4855-8737-89845ec1a49e', '4538ef51-e4f1-5fd9-86d4-ebe65cbd700d'],
        metric: ['radiation', 'temperature'],
        aggregate: 'max'
      }
    };

    expect(() => {
      expect(SensorMetricQueryRequestSchema.parse(req)).toMatchObject({ query: { since: 1 } });
    }).not.toThrow();
  });

  test('should accept valid GET query', () => {
    const req = {
      query: {
        sensorId: ['4e2a6260-1add-4855-8737-89845ec1a49e', '4538ef51-e4f1-5fd9-86d4-ebe65cbd700d'],
        metric: ['radiation', 'temperature'],
        aggregate: 'max',
        since: 2
      }
    };

    expect(() => {
      SensorMetricQueryRequestSchema.parse(req);
    }).not.toThrow();
  });
});
