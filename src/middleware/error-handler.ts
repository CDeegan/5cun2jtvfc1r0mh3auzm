import { Request, Response, NextFunction } from 'express';
import { Boom } from '@hapi/boom';
import { ZodError } from 'zod';

interface ExpressParseError extends SyntaxError {
  status: number;
}

export function expressErrorHandler(
  err: Error,
  req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next: NextFunction
) {
  if (err instanceof ZodError) {
    // validation errors
    return res.status(400).json({ errors: err.issues });
  } else if (err instanceof Boom) {
    // http errors
    if (err.isServer) {
      console.error(err);
    }
    return res.status(err.output.statusCode).json(err.output.payload);
  } else if (err instanceof SyntaxError && (err as ExpressParseError).status === 400) {
    // handle body-parser malformed errors
    return res.status(400).send({ errors: [err.message] });
  } else {
    // anything else
    console.error(err.stack);
    return res.status(500).send();
  }
}
