import dotenv from 'dotenv';
dotenv.config({ path: '.env' });
import express from 'express';
import router from '@code/route';
import { expressErrorHandler } from './middleware/error-handler';

const app = express();

app.use(express.json());

app.listen(process.env.WEB_PORT, () => {
  console.log(`API ready on port ${process.env.WEB_PORT}`);
});

router({ app });

app.use(expressErrorHandler);
